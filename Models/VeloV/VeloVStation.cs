﻿using Newtonsoft.Json;

namespace ms2i_ia_proba.Models.VeloV
{
    class VeloVStation
    {
        [JsonProperty("number")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("lng")]
        public double Longitude { get; set; }
        [JsonProperty("lat")]
        public double Latitude { get; set; }
        [JsonProperty("available_bikes")]
        public int AvailableBikes { get; set; }
    } 

}
