﻿using Newtonsoft.Json;

namespace ms2i_ia_proba.Models.ObservedData
{
    class ObservedData
    {
        [JsonProperty("number")]
        public int Id { get; set; }
        [JsonProperty("data")]
        public double[] Data { get; set; }
    }
}
