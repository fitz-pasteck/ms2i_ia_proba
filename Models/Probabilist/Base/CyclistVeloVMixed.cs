﻿using Microsoft.ML.Probabilistic.Algorithms;
using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Math;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Base
{
    class CyclistVeloVMixed
    {
        public InferenceEngine InferEngine;

        public int NodeCount = 2;

        protected VariableArray<Gaussian> MeansAPriori;
        protected VariableArray<Gamma> NoisesAPriori;
        protected Variable<Dirichlet> MixedAPriori;

        protected VariableArray<double> Means;
        protected VariableArray<double> Noises;
        protected Variable<Vector> Mixed;

        public virtual void BuildBayesianModel()
        {
            Range nodeIndicator = new Range(NodeCount);
            InferEngine = new InferenceEngine(new VariationalMessagePassing());

            MeansAPriori = Variable.Array<Gaussian>(nodeIndicator);
            NoisesAPriori = Variable.Array<Gamma>(nodeIndicator);
            Means = Variable.Array<double>(nodeIndicator);
            Noises = Variable.Array<double>(nodeIndicator);

            using (Variable.ForEach(nodeIndicator))
            {
                Means[nodeIndicator] = Variable<double>.Random(MeansAPriori[nodeIndicator]);
                Noises[nodeIndicator] = Variable<double>.Random(NoisesAPriori[nodeIndicator]);
            }

            MixedAPriori = Variable.New<Dirichlet>();
            Mixed = Variable<Vector>.Random(MixedAPriori);
            Mixed.SetValueRange(nodeIndicator);
        }

        public virtual void GuessDistrib(CyclistMixedData data)
        {
            MeansAPriori.ObservedValue = data.MeanDistrib;
            NoisesAPriori.ObservedValue = data.NoiseDistrib;
            MixedAPriori.ObservedValue = data.MixedDistrib;
        }
    }
}
