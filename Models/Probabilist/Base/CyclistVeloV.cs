﻿using Microsoft.ML.Probabilistic.Algorithms;
using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Base
{
    class CyclistVeloV
    {
        public InferenceEngine InferEngine;

        protected Variable<double> Mean;
        protected Variable<double> Noise;

        protected Variable<Gaussian> MeanAPriori;
        protected Variable<Gamma> NoiseAPriori;

        public virtual void BuildBayesianModel()
        {
            // Estimation
            MeanAPriori = Variable.New<Gaussian>();
            NoiseAPriori = Variable.New<Gamma>();
            // Inférence
            Mean = Variable.Random<double, Gaussian>(MeanAPriori);
            Noise = Variable.Random<double, Gamma>(NoiseAPriori);
            if (InferEngine == null)
            {
                InferEngine = new InferenceEngine(new ExpectationPropagation());
            }
        }

        public virtual void GuessDistrib(CyclistData data)
        {
            MeanAPriori.ObservedValue = data.MeanDistrib;
            NoiseAPriori.ObservedValue = data.NoiseDistrib;
        }
    }
}
