﻿using Microsoft.ML.Probabilistic.Algorithms;
using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Math;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Base
{
    class StationAvailabilityBase
    {
        public InferenceEngine InferEngine;

        public int ComponentsCount = 2;

        protected VariableArray<Gaussian> MeansAPriori;
        protected VariableArray<Gamma> NoisesAPriori;
        protected Variable<Dirichlet> MoodAPriori;

        protected VariableArray<double> Means;
        protected VariableArray<double> Noises;
        protected Variable<Vector> Mood;

        public virtual void BuildBayesianModel()
        {
            Range componentsIndicator = new Range(ComponentsCount);
            if (InferEngine == null)
            {
                InferEngine = new InferenceEngine(new VariationalMessagePassing());
            }

            MeansAPriori = Variable.Array<Gaussian>(componentsIndicator);
            NoisesAPriori = Variable.Array<Gamma>(componentsIndicator);
            Means = Variable.Array<double>(componentsIndicator);
            Noises = Variable.Array<double>(componentsIndicator);
            using (Variable.ForEach(componentsIndicator))
            {
                Means[componentsIndicator] = Variable<double>.Random(MeansAPriori[componentsIndicator]);
                Noises[componentsIndicator] = Variable<double>.Random(NoisesAPriori[componentsIndicator]);
            }

            MoodAPriori = Variable.New<Dirichlet>();
            Mood = Variable<Vector>.Random(MoodAPriori);
            Mood.SetValueRange(componentsIndicator);
        }

        public virtual void GuessDistrib(StationAvailabilityData data)
        {
            MeansAPriori.ObservedValue = data.MeansDistribs;
            NoisesAPriori.ObservedValue = data.NoisesDistribs;
            MoodAPriori.ObservedValue = data.MoodDistrib;
        }
    }
}
