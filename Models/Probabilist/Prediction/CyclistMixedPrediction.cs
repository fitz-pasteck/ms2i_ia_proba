﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;

namespace ms2i_ia_proba.Models.Probabilist.Prediction
{
    class CyclistMixedPrediction : CyclistVeloVMixed
    {
        private Gaussian TomorrowDistrib;
        public Variable<double> TomorrowTime;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();
            Variable<int> NodesIndicator = Variable.Discrete(Mixed);
            TomorrowTime = Variable.New<double>();
            using (Variable.Switch(NodesIndicator))
            {
                TomorrowTime.SetTo(
                    Variable.GaussianFromMeanAndPrecision(
                        Means[NodesIndicator],
                        Noises[NodesIndicator]
                    )
                );
            }
        }

        public Gaussian EstimateTomorrowDuration()
        {
            TomorrowDistrib = InferEngine.Infer<Gaussian>(TomorrowTime);
            return TomorrowDistrib;
        }

    }
}
