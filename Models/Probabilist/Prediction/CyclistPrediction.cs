﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;

namespace ms2i_ia_proba.Models.Probabilist.Prediction
{
    class CyclistPrediction : CyclistVeloV
    {
        private Gaussian TomorowDistrib;
        public Variable<double> TomorowDuration;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();
            TomorowDuration = Variable.GaussianFromMeanAndPrecision(Mean, Noise);
        }

        public Gaussian EstimateTomorowDuration()
        {
            TomorowDistrib = InferEngine.Infer<Gaussian>(TomorowDuration);
            return TomorowDistrib;
        }

        public Bernoulli EstimateTomorowDurationLesserThan(double duration)
        {
            return InferEngine.Infer<Bernoulli>(TomorowDuration < duration);
        }
    }
}
