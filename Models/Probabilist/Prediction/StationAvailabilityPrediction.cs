﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;

namespace ms2i_ia_proba.Models.Probabilist.Prediction
{
    class StationAvailabilityPrediction : StationAvailabilityBase
    {
        private Gaussian StationAvailabilityDistrib;
        public Variable<double> StationAvailability;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();
            Variable<int> componentsIndicator = Variable.Discrete(Mood);
            StationAvailability = Variable.New<double>();
            using (Variable.Switch(componentsIndicator))
            {
                StationAvailability.SetTo(
                    Variable.GaussianFromMeanAndPrecision(
                        Means[componentsIndicator], Noises[componentsIndicator]));
            }
        }

        public Gaussian EstimateStationAvailability()
        {
            StationAvailabilityDistrib = InferEngine.Infer<Gaussian>(StationAvailability);
            return StationAvailabilityDistrib;
        }
    }
}
