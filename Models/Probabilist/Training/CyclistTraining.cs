﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Training
{
    class CyclistTraining : CyclistVeloV
    {
        protected VariableArray<double> RecupVeloVDurations;
        protected Variable<int> RecupVeloVCount;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();
            RecupVeloVCount = Variable.New<int>();
            Range indRecupVeloVDuration = new Range(RecupVeloVCount);
            RecupVeloVDurations = Variable.Array<double>(indRecupVeloVDuration);
            using (Variable.ForEach(indRecupVeloVDuration))
            {
                RecupVeloVDurations[indRecupVeloVDuration] = Variable.GaussianFromMeanAndPrecision(Mean, Noise);
            }
        }

        public CyclistData PosteriorsCalculation(double[] observedData)
        {
            CyclistData posteriors = new CyclistData();
            RecupVeloVCount.ObservedValue = observedData.Length;
            RecupVeloVDurations.ObservedValue = observedData;
            posteriors.MeanDistrib = InferEngine.Infer<Gaussian>(Mean);
            posteriors.NoiseDistrib = InferEngine.Infer<Gamma>(Noise);
            return posteriors;
        }
    }
}
