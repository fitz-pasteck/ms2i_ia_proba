﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Training
{
    class StationAvailabilityTraining : StationAvailabilityBase
    {
        protected VariableArray<double> StationRefreshTimes;
        protected VariableArray<int> StationRefreshComponents;
        protected Variable<int> StationRefreshCount;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();

            StationRefreshCount = Variable.New<int>();
            Range stationRefreshRange = new Range(StationRefreshCount);

            StationRefreshTimes = Variable.Array<double>(stationRefreshRange);
            StationRefreshComponents = Variable.Array<int>(stationRefreshRange);

            using (Variable.ForEach(stationRefreshRange))
            {
                StationRefreshComponents[stationRefreshRange] = Variable.Discrete(Mood);
                Variable<int> switchPoint = StationRefreshComponents[stationRefreshRange];
                using (Variable.Switch(switchPoint))
                {
                    StationRefreshTimes[stationRefreshRange].SetTo(
                        Variable.GaussianFromMeanAndPrecision(
                            Means[switchPoint], Noises[switchPoint]));
                }
            }
        }

        public StationAvailabilityData PosteriorsCalculation(double[] observedData) {
            StationAvailabilityData posteriors = new StationAvailabilityData();
            StationRefreshCount.ObservedValue = observedData.Length;
            StationRefreshTimes.ObservedValue = observedData;
            posteriors.MeansDistribs = InferEngine.Infer<Gaussian[]>(Means);
            posteriors.NoisesDistribs = InferEngine.Infer<Gamma[]>(Noises);
            posteriors.MoodDistrib = InferEngine.Infer<Dirichlet>(Mood);
            return posteriors;
        }
    }
}
