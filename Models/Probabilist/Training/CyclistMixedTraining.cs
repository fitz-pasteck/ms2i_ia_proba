﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using ms2i_ia_proba.Models.Probabilist.Base;
using ms2i_ia_proba.Models.Probabilist.Data;

namespace ms2i_ia_proba.Models.Probabilist.Training
{
    class CyclistMixedTraining : CyclistVeloVMixed
    {
        protected Variable<int> TrajectsCount;

        protected VariableArray<double> TrajectsTimes;
        protected VariableArray<int> TrajectsNodes;

        public override void BuildBayesianModel()
        {
            base.BuildBayesianModel();
            TrajectsCount = Variable.New<int>();
            Range trajectsIndicator = new Range(TrajectsCount);
            TrajectsTimes = Variable.Array<double>(trajectsIndicator);
            TrajectsNodes = Variable.Array<int>(trajectsIndicator);

            using (Variable.ForEach(trajectsIndicator))
            {
                TrajectsNodes[trajectsIndicator] = Variable.Discrete(Mixed);
                using (Variable.Switch(TrajectsNodes[trajectsIndicator]))
                {
                    TrajectsTimes[trajectsIndicator].SetTo(
                        Variable.GaussianFromMeanAndPrecision(
                            Means[TrajectsNodes[trajectsIndicator]],
                            Noises[TrajectsNodes[trajectsIndicator]]
                        )    
                    );
                }
            }
        }

        public CyclistMixedData PosteriorsCalculation(double[] observedData)
        {
            CyclistMixedData posteriors = new CyclistMixedData();
            TrajectsCount.ObservedValue = observedData.Length;
            TrajectsTimes.ObservedValue = observedData;
            posteriors.MeanDistrib = InferEngine.Infer<Gaussian[]>(Means);
            posteriors.NoiseDistrib = InferEngine.Infer<Gamma[]>(Noises);
            posteriors.MixedDistrib = InferEngine.Infer<Dirichlet>(Mixed);
            return posteriors;
        }
    }
}
