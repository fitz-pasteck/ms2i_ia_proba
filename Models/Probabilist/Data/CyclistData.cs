﻿using Microsoft.ML.Probabilistic.Distributions;

namespace ms2i_ia_proba.Models.Probabilist.Data
{
    public class CyclistData
    {
        public Gaussian MeanDistrib;
        public Gamma NoiseDistrib;

        public CyclistData() {
            MeanDistrib = new Gaussian();
            NoiseDistrib = new Gamma();
        }

        public CyclistData(Gaussian mean, Gamma noise)
        {
            MeanDistrib = mean;
            NoiseDistrib = noise;
        }
    }
}