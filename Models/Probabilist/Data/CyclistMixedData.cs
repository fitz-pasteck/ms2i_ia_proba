﻿using Microsoft.ML.Probabilistic.Distributions;

namespace ms2i_ia_proba.Models.Probabilist.Data
{
    class CyclistMixedData
    {
        public Gaussian[] MeanDistrib;
        public Gamma[] NoiseDistrib;
        public Dirichlet MixedDistrib;

        public CyclistMixedData() { }
    }
}
