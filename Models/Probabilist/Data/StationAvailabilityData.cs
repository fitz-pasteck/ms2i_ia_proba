﻿using Microsoft.ML.Probabilistic.Distributions;

namespace ms2i_ia_proba.Models.Probabilist.Data
{
    class StationAvailabilityData
    {
        public Gaussian[] MeansDistribs;
        public Gamma[] NoisesDistribs;
        public Dirichlet MoodDistrib;

        public StationAvailabilityData() { }
    }
}
