﻿using Microsoft.ML.Probabilistic.Distributions;
using System;
using ms2i_ia_proba.Models.Probabilist.Data;
using ms2i_ia_proba.Models.Probabilist.Prediction;
using ms2i_ia_proba.Models.Probabilist.Training;

namespace ms2i_ia_proba.Launcher
{
    class TestVeloVProbability : IProbability
    {
        public void BuildProbability()
        {
            // 1 - Entrainement
            Console.WriteLine("\n1 - Training Phase : ");
            double[] data = { 13, 17, 20, 25, 16, 11, 16, 14, 12.5 };

            CyclistData myDistribs = new CyclistData(
                Gaussian.FromMeanAndPrecision(1, 0.01),
                Gamma.FromShapeAndRate(2, 0.5)
            );
            CyclistTraining myTraining = new CyclistTraining();
            myTraining.BuildBayesianModel();
            myTraining.GuessDistrib(myDistribs);
            CyclistData myPosterior = myTraining.PosteriorsCalculation(data);

            Console.WriteLine($"Mean posterior : {myPosterior.MeanDistrib.ToString()}");
            Console.WriteLine($"Noise posterior : {myPosterior.NoiseDistrib.ToString()}");

            // 2 - Predictions
            Console.WriteLine("\n2 - First prediction for tomorrow : ");
            CyclistPrediction myPrediction = new CyclistPrediction();
            myPrediction.BuildBayesianModel();
            myPrediction.GuessDistrib(myPosterior);
            Gaussian tomorrowDistrib = myPrediction.EstimateTomorowDuration();
            Console.WriteLine($"Tomorrow prediction : {tomorrowDistrib.ToString()}");
            Console.WriteLine($"Standard deviation (Ecart-type) : {Math.Sqrt(tomorrowDistrib.GetVariance())}");
            double maxDuration = 13;
            Console.WriteLine(
                $"Duration probability < {maxDuration} min : {myPrediction.EstimateTomorowDurationLesserThan(maxDuration)}"
            );

            // 3 - Apprentissage en ligne
            Console.WriteLine("\n3 - Learning phase : ");
            double[] nextWeekData = new double[] { 18, 25, 30, 14, 11 };
            myPrediction.GuessDistrib(myPosterior);
            CyclistData nextWeekPosterior = myTraining.PosteriorsCalculation(nextWeekData);
            Console.WriteLine($"Next week mean posterior : {nextWeekPosterior.MeanDistrib.ToString()}");
            Console.WriteLine($"Next week noise posterior : {nextWeekPosterior.NoiseDistrib.ToString()}");

            // 4 - Nouvelle prédictions pour demain
            Console.WriteLine("\n4 - New prediction for tomorrow : ");
            myPrediction.GuessDistrib(nextWeekPosterior);
            tomorrowDistrib = myPrediction.EstimateTomorowDuration();
            Console.WriteLine($"Tomorrow prediction : {tomorrowDistrib.ToString()}");
            Console.WriteLine($"Standard deviation (Ecrat-type) : {Math.Sqrt(tomorrowDistrib.GetVariance())}");
            maxDuration = 18;
            Console.WriteLine(
                $"Duration probability < {maxDuration} min : {myPrediction.EstimateTomorowDurationLesserThan(maxDuration)}"
            );
        }
    }
}
