﻿using Microsoft.ML.Probabilistic.Distributions;
using System;
using ms2i_ia_proba.Models.Probabilist.Data;
using ms2i_ia_proba.Models.Probabilist.Prediction;
using ms2i_ia_proba.Models.Probabilist.Training;

namespace ms2i_ia_proba.Launcher
{
    class TestVeloVMixedProbability : IProbability
    {
        public void BuildProbability()
        {
            // 1 - Entrainement
            Console.WriteLine("\n1 - Training Phase : ");
            double[] trajectsData = { 13, 17, 20, 25, 16.5, 11, 16, 25, 12.5, 30 };

            CyclistMixedData myDistribsAPriori = new CyclistMixedData
            {
                MeanDistrib = new Gaussian[] {
                    new Gaussian(15, 100),
                    new Gaussian(30, 100)
                },
                NoiseDistrib = new Gamma[]
                {
                    new Gamma(2, 0.5),
                    new Gamma(2, 0.5)
                },
                MixedDistrib = new Dirichlet(1, 1)
            };

            CyclistMixedTraining myTraining = new CyclistMixedTraining();
            myTraining.BuildBayesianModel();
            myTraining.GuessDistrib(myDistribsAPriori);

            CyclistMixedData posteriors = myTraining.PosteriorsCalculation(trajectsData);

            for (int i = 0; i < myTraining.NodeCount; i++)
            {
                Console.WriteLine($"Posterior Mean n°{i} : {posteriors.MeanDistrib[i].ToString()}");
                Console.WriteLine($"Posterior Noise n°{i} : {posteriors.NoiseDistrib[i].ToString()}");
            }
            Console.WriteLine($"Mixed factor : {posteriors.MixedDistrib}");

            // 2 - Predictions
            Console.WriteLine("\n2 - First prediction for tomorrow : ");
            CyclistMixedPrediction myPrediction = new CyclistMixedPrediction();
            myPrediction.BuildBayesianModel();
            myPrediction.GuessDistrib(posteriors);
            Gaussian tomorrowDistrib = myPrediction.EstimateTomorrowDuration();
            Console.WriteLine($"Tomorrow prediction : {tomorrowDistrib.ToString()}");
            Console.WriteLine($"Standard deviation (Ecart-type) : {Math.Sqrt(tomorrowDistrib.GetVariance())}");

            // 3 - Apprentissage en ligne
            Console.WriteLine("\n3 - Learning phase : ");
            double[] nextWeekData = new double[] { 18, 25, 30, 14, 11 };
            myPrediction.GuessDistrib(posteriors);
            CyclistMixedData nextWeekPosterior = myTraining.PosteriorsCalculation(nextWeekData);
            for (int i = 0; i < myTraining.NodeCount; i++)
            {
                Console.WriteLine($"Posterior Mean n°{i} : {nextWeekPosterior.MeanDistrib[i].ToString()}");
                Console.WriteLine($"Posterior Noise n°{i} : {nextWeekPosterior.NoiseDistrib[i].ToString()}");
            }
            Console.WriteLine($"Mixed factor : {nextWeekPosterior.MixedDistrib}");

            // 4 - Nouvelle prédictions pour demain
            Console.WriteLine("\n4 - New prediction for tomorrow : ");
            myPrediction.GuessDistrib(nextWeekPosterior);
            tomorrowDistrib = myPrediction.EstimateTomorrowDuration();
            Console.WriteLine($"Tomorrow prediction : {tomorrowDistrib.ToString()}");
            Console.WriteLine($"Standard deviation (Ecart-type) : {Math.Sqrt(tomorrowDistrib.GetVariance())}");

        }
    }
}
