﻿using Microsoft.ML.Probabilistic.Distributions;
using Microsoft.ML.Probabilistic.Models;
using System;

namespace ms2i_ia_proba.Launcher
{
    class TestTrajectProbability : IProbability
    {
        public void BuildProbability()
        {
            // Estimation de la durée moyenne du trajet
            Double dureeMoyenneTrajet = 15;

            // Variables aléatoires
            Variable<double> dureeMoyenne = Variable.GaussianFromMeanAndPrecision(dureeMoyenneTrajet, 0.01);
            Variable<double> bruitTrafic = Variable.GammaFromShapeAndScale(2, 0.5);
            Variable<double> dureeLundi = Variable.GaussianFromMeanAndPrecision(dureeMoyenne, bruitTrafic);
            Variable<double> dureeMardi = Variable.GaussianFromMeanAndPrecision(dureeMoyenne, bruitTrafic);
            Variable<double> dureeMercredi = Variable.GaussianFromMeanAndPrecision(dureeMoyenne, bruitTrafic);

            // Valeurs observées
            dureeLundi.ObservedValue = 13;
            dureeMardi.ObservedValue = 17;
            dureeMercredi.ObservedValue = 16;

            // Inférence : Opération logique par laquelle on admet une proposition en vertu de sa liaison avec d'autres propositions déjà tenues pour vraies.
            InferenceEngine engine = new InferenceEngine();
            Gaussian moyennePost = engine.Infer<Gaussian>(dureeMoyenne);
            Gamma bruitPost = engine.Infer<Gamma>(bruitTrafic);
            Console.WriteLine($"moyenne postérieure : {moyennePost.ToString()}");
            Console.WriteLine($"bruit postérieure : {bruitPost.ToString()}");

            // Prédictions de la durée du prochain trajet
            Variable<double> dureeDemain = Variable.GaussianFromMeanAndPrecision(dureeMoyenne, bruitTrafic);
            Gaussian distribDemain = engine.Infer<Gaussian>(dureeDemain);
            Console.WriteLine($"Prédiction demain : {distribDemain.ToString()} // Ecart type : {Math.Sqrt(distribDemain.GetVariance())} ");

            // Probabilité que le prochain trajet mette moins de 18 minutes
            double probMoinsDe18Mn = engine.Infer<Bernoulli>(dureeDemain < 18.0).GetProbTrue();
            Console.WriteLine("Probabilité que le trajet prenne moins de 18mn: {0:P2}", probMoinsDe18Mn);

            // Probabilité que le prochain trajet mette moins de 10 minutes
            double probMoinsDe10Mn = engine.Infer<Bernoulli>(dureeDemain < 10.0).GetProbTrue();
            Console.WriteLine("Probabilité que le trajet prenne moins de 10mn: {0:P2}", probMoinsDe10Mn);
        }

    }
}
