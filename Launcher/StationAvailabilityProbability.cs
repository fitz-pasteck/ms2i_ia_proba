﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ML.Probabilistic.Distributions;
using ms2i_ia_proba.Models.VeloV;
using ms2i_ia_proba.Repository;
using ms2i_ia_proba.Models.Probabilist.Data;
using ms2i_ia_proba.Models.Probabilist.Training;
using ms2i_ia_proba.Models.Probabilist.Prediction;
using ms2i_ia_proba.Models.ObservedData;

namespace ms2i_ia_proba.Launcher
{
    class StationAvailabilityProbability : IProbability
    {
        public void BuildProbability()
        {
            // Récupération des données des stations de VeloV dans Lyon 4
            VeloVStationRepository vvsRepository = new VeloVStationRepository();
            Task<string> veloVStationRawData = vvsRepository.GetVeloVStationDataAsync();
            veloVStationRawData.Wait();
            List<VeloVStation> stations = vvsRepository.GetVeloVStations(veloVStationRawData.Result);

            // Récupération des 5 plus proches stations de VeloV
            GeoCoordinate myPosition = new GeoCoordinate(45.7775267, 4.8264393);
            List<GeoCoordinate> availableVeloVStationPositions = stations
                .Where(s => s.AvailableBikes > 0)
                .Select(s => new GeoCoordinate(s.Latitude, s.Longitude))
                .ToList()
                .OrderBy(geo => geo.GetDistanceTo(myPosition))
                .Take(5)
                .ToList();
            List<VeloVStation> observedStations = new List<VeloVStation>();
            foreach (GeoCoordinate coord in availableVeloVStationPositions)
            {
                VeloVStation station = stations
                    .Where(s => (s.Latitude == coord.Latitude && s.Longitude == coord.Longitude))
                    .First();
                observedStations.Add(station);
                Console.WriteLine($"Observed VeloV Station n°{station.Id} : {station.Name}");
            }

            // Récupération des données observées : le nombre de places par stations
            ObservedDataRepository odRepository = new ObservedDataRepository();
            List<ObservedData> observedData = odRepository.GetObservedData()
                .Where(od => observedStations.Select(os => os.Id).Contains(od.Id))
                .ToList();
            List<ObservedData> dataToPush = new List<ObservedData>();
            foreach (ObservedData observedStation in observedData)
            {
                Console.WriteLine("\n----------------------------------------------------------------------------------------");

                VeloVStation currentStation = observedStations
                    .Where(s => s.Id == observedStation.Id)
                    .First();

                List<double> observedAvailability = observedStation.Data.ToList();
                observedAvailability.Add(currentStation.AvailableBikes);
                observedStation.Data = observedAvailability.ToArray();

                Console.WriteLine($"Station {observedStation.Id} with initial data : {String.Join(", ", observedStation.Data)}");

                // Phase d'analyse
                AnalyzeData(observedStation.Data);

                dataToPush.Add(observedStation);

                Console.WriteLine("----------------------------------------------------------------------------------------");
            }
            odRepository.PushObservedData(dataToPush);
        }

        private void AnalyzeData(double[] data)
        {
            // 1 - Entrainement
            Console.WriteLine("\n1 - Training Phase : ");

            StationAvailabilityData myDistribsAPriori = new StationAvailabilityData
            {
                MeansDistribs = new Gaussian[] {
                    new Gaussian(10, 100),
                    new Gaussian(30, 100)
                },
                NoisesDistribs = new Gamma[]
                {
                    new Gamma(2, 0.5),
                    new Gamma(2, 0.5)
                },
                MoodDistrib = new Dirichlet(1, 1)
            };

            StationAvailabilityTraining myTraining = new StationAvailabilityTraining();
            myTraining.BuildBayesianModel();
            myTraining.GuessDistrib(myDistribsAPriori);

            StationAvailabilityData posteriors = myTraining.PosteriorsCalculation(data);

            // 2 - Predictions
            Console.WriteLine("\n2 - Prediction for next time : ");
            StationAvailabilityPrediction myPrediction = new StationAvailabilityPrediction();
            myPrediction.BuildBayesianModel();
            myPrediction.GuessDistrib(posteriors);

            Gaussian nextDistrib = myPrediction.EstimateStationAvailability();
            Console.WriteLine($"Next time prediction : {nextDistrib.ToString()}");

        }
    }
}
