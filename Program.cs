﻿using System;
using ms2i_ia_proba.Launcher;

namespace ms2i_ia_proba
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choisissez le mode de lancement de l'application : ");
            // TP DureeCyclist[1|2|3]
            Console.WriteLine("1 - (TP) 1er test classique d'un IA probabiliste");
            Console.WriteLine("2 - (TP) Test d'une IA probabiliste qui calcule les futures durées des trajets d'un cycliste");
            Console.WriteLine("3 - (TP) Test d'une IA probabiliste mixte qui calcule les futures durées des trajets d'un cycliste de manière plus naturelle");
            // Projet étude du taux de rafraichissement de borne VeloV
            Console.WriteLine("4 - (Projet) Etude du taux de rafraichissement de borne VeloV (en fonction du traffic et de l'humeur du chauffeur)");
            string response = Console.ReadLine();
            bool successParse = Int32.TryParse(response, out int choice);
            IProbability probabilityToLaunch = null;
            if (successParse)
            {
                switch (choice)
                {
                    case 1 :
                        probabilityToLaunch = new TestTrajectProbability();
                        break;
                    case 2 :
                        probabilityToLaunch = new TestVeloVProbability();
                        break;
                    case 3 :
                        probabilityToLaunch = new TestVeloVMixedProbability();
                        break;
                    case 4 :
                        probabilityToLaunch = new StationAvailabilityProbability();
                        break;
                    default:
                        Console.WriteLine("Choix non valide.");
                        break;
                }
            }
            else
            {
                Console.WriteLine("Choix non valide.");
            }

            if (probabilityToLaunch != null) probabilityToLaunch.BuildProbability();
            //-------------------------------------------------------------------------------------
            Console.ReadKey();
        }

    }
}
