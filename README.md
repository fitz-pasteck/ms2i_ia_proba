# ms2i_ia_proba

Projet d'IA pour ma dernière année en Ms2i.

Ce projet consiste en l'étude des taux de rafraichissement des bornes VeloV à
Lyon.

Pour ce projet, nous simulons un utilisateur qui se trouve à la Croix-Rousse et 
nous lui indiquons la probabilité du remplissage des 5 stations VeloV qui se
trouvent dans un rayon de 5 km autour de lui.

## IA Probabiliste
Pour la partie probabilité de cette IA, j'ai utilisé le framewrok Infer.NET.

J'observe la variable du taux de rafraichissement des bornes VeloV à Lyon en
analysant des variables aléatoires qui sont le traffic et l'humeur des utilisateurs
de VeloV.

Voici les distributions utilisées pour chaque variable :
 * taux de raffraichissement des bornes Velov --> Gaussienne
 * Traffic --> Gamma
 * Humeur des utilisateur --> Dirichlet

Les données utilisées pour les observations sont en 2 parties :
 * Des données que j'ai choisi arbitrairement qui représente un nombre de places
disponible
 * Le nombres de places disponibles pour chaque borne VeloV dont les informations
sont mises à disposition par DataGrandLyon

Ces données sont concaténées dans le fichier Data/observed_data.json du projet.
Au lancement de l'application, on récupère ces données pour faire les prédictions.

## Modes de lancements
4 modes de lancement sont présents :
 * les 3 premiers concernent un TP mis à disposition par Infer.NET
 * le dernier est le projet d'IA

## Configuration
2 valeurs sont à paramétrer dans le fichier App.config à la racine de l'application :
```xml
<add key="data_directory" value="{chemin absolu vers le fichier observed_data.json qui se trouve dans le répertoire Data}" />
<add key="data_filename" value="{nom du fichier observed_data.json}" />
```