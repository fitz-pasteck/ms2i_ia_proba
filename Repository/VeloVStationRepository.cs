﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using ms2i_ia_proba.Models.VeloV;

namespace ms2i_ia_proba.Repository
{
    class VeloVStationRepository : DataAccess
    {
        
        public async Task<string> GetVeloVStationDataAsync()
        {
            String responseBody = "";
            using (Client)
            {
                try
                {
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthenticationString());
                    Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string url = HttpUtility.UrlDecode(GetVeloVStationAccessUrl());
                    HttpResponseMessage response = await Client.GetAsync(url);
                    response.EnsureSuccessStatusCode();
                    responseBody = await response.Content.ReadAsStringAsync();
                }
                catch(HttpRequestException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                }

            }
            return responseBody;
        }

        public List<VeloVStation> GetVeloVStations(string rawData)
        {
            JObject o = JObject.Parse(rawData);
            JToken[] values = o["values"].ToArray();
            List<VeloVStation> stations = new List<VeloVStation>();
            foreach (JToken value in values) {
                VeloVStation station = JsonConvert.DeserializeObject<VeloVStation>(value.ToString());
                stations.Add(station);
            }
            return stations;
        }

    }
}
