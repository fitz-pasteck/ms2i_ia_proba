﻿using System;
using System.Net.Http;
using System.Text;
using System.Web;

namespace ms2i_ia_proba.Repository
{
    class DataAccess
    {
        private const string LOGIN = "projet_transverse_2@outlook.fr";
        private const string PASSWORD = "Mdp!Tr@sv3rs3*";
        private const string BASE_URL_ACCESS = "https://download.data.grandlyon.com/ws/";
        private const string SERVICE = "rdata";
        private const string TABLE = "jcd_jcdecaux";
        private const string TABLE_SCHEME = "jcdvelov";
        private const string JSON_FILE = "all.json";

        private readonly HttpClient client;

        public HttpClient Client { get => client; }

        public DataAccess()
        {
            client = new HttpClient();
        }

        public String GetAuthenticationString()
        {
            return BuildAuthenticationString(LOGIN, PASSWORD);
        }

        public String GetBaseDataAccessUrl()
        {
            return BuildRequestBaseURL(BASE_URL_ACCESS, SERVICE, TABLE, TABLE_SCHEME, JSON_FILE);
        }

        public string GetVeloVStationAccessUrl()
        {
            var queryBuilder = new UriBuilder(GetBaseDataAccessUrl())
            {
                Port = -1
            };
            var query = HttpUtility.ParseQueryString(queryBuilder.Query);
            query["field"] = "commune";
            query["value"] = "Lyon 4 ème";
            queryBuilder.Query = query.ToString();
            return queryBuilder.ToString();
        }

        private String BuildRequestBaseURL(string baseUrl,
                                          string service,
                                          string table,
                                          string tableScheme,
                                          string targetedFile)
        {
            String url = "";
            if (baseUrl != null && service != null && targetedFile != null)
            {
                if (table != null && tableScheme != null)
                {
                    url = String.Format("{0}{1}/{2}.{3}/{4}", baseUrl, service, table, tableScheme, targetedFile);
                }
                else
                {
                    url = String.Format("{0}{1}/{2}", baseUrl, service, targetedFile);
                }
            }
            return url;
        }

        private String BuildAuthenticationString(string login, string password)
        {
            String authString = "";
            if (login != null && password != null)
            {
                String preEncodedAuthString = $"{login}:{password}";
                byte[] preEncodedAuthStringBytes = Encoding.UTF8.GetBytes(preEncodedAuthString);
                authString = Convert.ToBase64String(preEncodedAuthStringBytes);
            }
            return authString;
        }
    }
}
