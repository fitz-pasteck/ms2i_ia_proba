﻿using ms2i_ia_proba.Models.ObservedData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;

namespace ms2i_ia_proba.Repository
{
    class ObservedDataRepository
    {
        private const string DATA_DIRECTORY = "data_directory";
        private const string DATA_FILENAME = "data_filename";

        private string GetDataFilePath()
        {
            string directory = ConfigurationManager.AppSettings.Get(DATA_DIRECTORY);
            string filename = ConfigurationManager.AppSettings.Get(DATA_FILENAME);
            if (directory == null || filename == null)
            {
                throw new FileNotFoundException("Configuration file doesn't contain necessary information about the data file.");
            }
            return $"{directory}/{filename}";
        }

        public List<ObservedData> GetObservedData()
        {
            List<ObservedData> data = new List<ObservedData>();
            using (StreamReader reader = new StreamReader(GetDataFilePath()))
            {
                string json = reader.ReadToEnd();
                JObject o = JObject.Parse(json);
                JToken[] values = o["observed_stations"].ToArray();
                foreach (JToken value in values)
                {
                    ObservedData observedStation = JsonConvert.DeserializeObject<ObservedData>(value.ToString());
                    data.Add(observedStation);
                }
            }
            return data;
        }

        public void PushObservedData(List<ObservedData> dataToPush)
        {
            List<ObservedData> data = GetObservedData();
            DataSet dataSet = new DataSet("dataSet")
            {
                Namespace = "NetFramework"
            };
            DataTable table = new DataTable("observed_stations");
            DataColumn columnNumber = new DataColumn("number", typeof(int));
            DataColumn columnData = new DataColumn("data", typeof(double[]));
            table.Columns.Add(columnNumber);
            table.Columns.Add(columnData);
            dataSet.Tables.Add(table);
            foreach (ObservedData observedData in data) {
                if (dataToPush.Select(dtp => dtp.Id).ToList().Contains(observedData.Id))
                {
                    observedData.Data = dataToPush
                        .Where(dtp => dtp.Id == observedData.Id)
                        .First()
                        .Data;
                }
                DataRow row = table.NewRow();
                row["number"] = observedData.Id;
                row["data"] = observedData.Data;
                table.Rows.Add(row);
            }
            dataSet.AcceptChanges();
            string jsonToPush = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            File.WriteAllText(GetDataFilePath(), jsonToPush);
        }
    }
}
